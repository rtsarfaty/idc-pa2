package hmm.train;
 
import hmm.NGram;
import hmm.Symbol;

import java.util.ArrayList; 
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeSet;
 
import utils.CountMap; 
import utils.LineReader;
import utils.LineWriter; 
 
public class TrainHMM {

	
	public static void main(String[] args) {

		if (args.length != 3) 
		{
			System.err.println("Usage:  TrainHMM  <treebank_path> <y|n> <model>");
			return;
		} 
		// read command line args
		System.out.println("Training step 1: reading command line args ... ");

		String sTrainSet =  args[0];
		String sSmooth = args[1];
		String sModel = args[2];
		int iModel = Integer.valueOf(sModel).intValue();

		boolean bSmooth = false;
		int iThreshold = 0;
		int sentences = 0;
		int segments = 0;

		if (sSmooth.equals("y"))
		{
			bSmooth = true;
			iThreshold = 1;
		}

		// upload the data file into a collection of tagged sequences
		System.out.println("Training step 2: reading tagged sequences ... ");
		// all sentences in training
		ArrayList<TaggedSequence> theTaggedData = new ArrayList<TaggedSequence>();
		// counting the unique (w) words
		CountMap<String> cmSegs =  new CountMap<String>();


		TaggedSequence ts = new TaggedSequence();
		LineReader trainset = new LineReader(sTrainSet);
		String sLine = trainset.readLine();

		while (sLine != null) {
			sLine = sLine.trim();
			//System.out.println("["+sLine+"]");
			if (sLine.length() == 0)
			{
				// empty line --> interpret as sentence
				sentences++;
				theTaggedData.add(ts);
				// start new sentence
				ts = new TaggedSequence();
				sLine = trainset.readLine();
				continue;
			}
			else if (sLine.startsWith("#"))
			{
				// skip line 
				sLine = trainset.readLine();
				continue;
			} 
			else
			{
				// interpret line
				StringTokenizer st = new StringTokenizer(sLine," ");
				if (st.countTokens() != 2) 
				{
					System.err.println("number of tokens should be 2");
					throw new IllegalStateException("number of strings per line should be 2: "+sLine);
				}
				segments++;
				String seg = st.nextToken();
				String tag = st.nextToken();
				ts.add(new TaggedTerminal(seg, tag));
				cmSegs.increment(seg);
				sLine = trainset.readLine();
				continue;
			}
		}

		System.out.println("Training step 3: unknown words handling  ... "+sSmooth);
		
		if (bSmooth)
		{
			// collect rare words
			Set<String> unkWords = new HashSet<String>();
			for (String seg : cmSegs.keySet()) {
				int counter = cmSegs.get(seg).intValue();
				if (counter<=iThreshold)
				{
					System.out.println("Unknown words -- "+seg);
					unkWords.add(seg);
				}
			}
			// reset counts;
			cmSegs = new CountMap<String>();
			
			// set as unknown in tagged data
			for (TaggedSequence sent : theTaggedData) {
				for (TaggedTerminal tt : sent.getTaggedSequence()) {
					if (unkWords.contains(tt.getTerminal()))
					{
						tt.setTerminal(Symbol._unk);
					}
				}
			}
		}
		
		System.out.println("Training step 4: learning model parameters");
		// initiating all lexical entries (all (t,w) per w)
		HashMap<String, Set<String>> theTaggedTerminalsPerWord = new HashMap<String, Set<String>>();
		// initiating the unique (w,t) pairs
		CountMap<TaggedTerminal> cmTaggedTerminals = new CountMap<TaggedTerminal>();
		// initiating all language models
		HashMap<Integer, TrainLM> allNGrams = new HashMap<Integer, TrainLM>();
		for (int i = 1; i <= iModel; i++) {
			TrainLM nGrams = new TrainLM(i);
			allNGrams.put(new Integer(i), nGrams);
		}
		
		for (TaggedSequence taggedSequence : theTaggedData) {
			
			// train language models
			ArrayList<String> theTags = taggedSequence.getTags();
			for (int i = 1; i <= iModel; i++) {
				TrainLM ngramTrained = allNGrams.get(new Integer(i));
				ngramTrained.addNGrams(theTags);
			}
			
			// train lexical model
			Iterator<TaggedTerminal> taggedSegs = taggedSequence.getTaggedSequence().iterator();
			while (taggedSegs.hasNext()) {
				
				// read the tagged terminal
				TaggedTerminal tt = (TaggedTerminal) taggedSegs.next();
				String seg = tt.getTerminal();

				// count segs and seg-tag entries
				cmSegs.increment(seg);
				cmTaggedTerminals.increment(tt);
				
				// update the lexicon
				if (!theTaggedTerminalsPerWord.containsKey(seg)){
					theTaggedTerminalsPerWord.put(seg, new HashSet<String>());	
				}
				theTaggedTerminalsPerWord.get(seg).add(tt.getPOStag());
			}
		}
		
		System.out.println(" Processed "+sentences+" sentences ");
		System.out.println(" -- Number of segment types "+cmSegs.entrySet().size());
		System.out.println(" -- Number of segment tokens "+cmSegs.allCounts());
		System.out.println(" -- Number of tagged-terminal types "+cmTaggedTerminals.entrySet().size());
		System.out.println(" -- Number of tagged-terminal tokens "+cmTaggedTerminals.allCounts());
		System.out.println(" -- Number of tagged-sequences in training "+theTaggedData.size());

		// print the lex file
		System.out.println("Training step 5: printing emissions params");
		writeEmissions(sTrainSet, sSmooth, iModel,  
			cmTaggedTerminals, cmSegs, theTaggedTerminalsPerWord, allNGrams);

		// print the ngrams file
		System.out.println("Training step 6: printing transitions params");
		writeTransitions(sTrainSet, sSmooth, iModel, sentences, segments, allNGrams);
	}


	/****
	 * Write emission probabilities into a files
	 * 
	 * @param sTrainSet
	 * @param sSmooth
	 * @param iModel
	 * @param theTaggedTerminalsPerWord
	 * @param cmTaggedTerminals
	 * @param cmSegs
	 * @param allNGrams
	 * @return
	 */
	private static void writeEmissions(String sTrainSet, 
			String sSmooth, 
			int iModel, // for header 
			CountMap<TaggedTerminal> cmTaggedTerminals,
			CountMap<String> cmSegs,
			HashMap<String, Set<String>> theTaggedTerminalsPerWord,
			HashMap<Integer, TrainLM> allNGrams) 
	{
		LineWriter lwEmissions = new LineWriter(sTrainSet+"-"+iModel+"-"+sSmooth+".lex");
		
		// create a map
		TrainLM unigramTrained = allNGrams.get(new Integer(1));
		Set<String> setSegs = new TreeSet<String>(cmSegs.keySet());
		Iterator<String> itSegs = setSegs.iterator();
		while (itSegs.hasNext()) 
		{
			StringBuffer sbe = new StringBuffer();
			String sSeg = itSegs.next();
			sbe.append(sSeg);
			Set<String> allTags = theTaggedTerminalsPerWord.get(sSeg);
			if (!theTaggedTerminalsPerWord.containsKey(sSeg))
			{
				throw new IllegalStateException("cannot find tags for "+sSeg);
			}
			Iterator<String> itAllTags = allTags.iterator();
			while (itAllTags.hasNext()) {
				String pos = (String) itAllTags.next();
				sbe.append("\t");
				sbe.append(pos);
				TaggedTerminal theTT = new TaggedTerminal(sSeg, pos);
				int countlex = cmTaggedTerminals.get(theTT).intValue();
				NGram n1 = new NGram(1);
				n1.add(pos);
				int countgram = unigramTrained.getCountMap().get(n1);
				double logprob = Math.log((double)countlex / (double)countgram);
				sbe.append("\t");
				sbe.append(logprob);
			}
			
			lwEmissions.writeLine(sbe.toString());
		}
		lwEmissions.close();
	}


	/***
	 * Write transition probabilities into a file
	 * 
	 * @param iModel
	 * @param sentences
	 * @param cmSegs
	 * @param allNGrams
	 * @param lwTransitions
	 */
	private static void writeTransitions(String sTrainSet, String sSmooth, int iModel, 
			int sentences, int segments, HashMap<Integer, TrainLM> allNGrams) 
	{
		// Header
		LineWriter lwTransitions = new LineWriter(sTrainSet+"-"+iModel+"-"+sSmooth+".gram");
		
		lwTransitions.writeLine("\\data\\");
		for (int i = 1; i <= iModel; i++) {
			TrainLM ngramTrained = allNGrams.get(new Integer(i));
			StringBuffer sbLine = new StringBuffer();
			sbLine.append("ngram ");
			sbLine.append(i);
			sbLine.append(" = ");
			sbLine.append(ngramTrained.getNumberOfNgrams());
			lwTransitions.writeLine(sbLine.toString());
		}
		lwTransitions.writeLine("");
		
		// trained ngrams
		for (int i = 1; i <= iModel; i++) {
			
			TrainLM ngramTrained = allNGrams.get(new Integer(i));
			TrainLM ngramTrainedHistory = allNGrams.get(new Integer(i-1));
			
			System.out.println(ngramTrained.getN());
			System.out.println(ngramTrained.getCountMap().keySet());
			
			StringBuffer sbLine = new StringBuffer();
			sbLine.append("\\");
			sbLine.append(i);
			sbLine.append("-grams\\");
			lwTransitions.writeLine(sbLine.toString());

			System.out.println(sbLine.toString());
			
			int totalcountfuture = 0;
			double totalcountlog = 0;
			
			Set<NGram> setGrams = new TreeSet<NGram>(ngramTrained.cmNGramCounts.keySet());
			Iterator<NGram> itNgrams = setGrams.iterator();
			while (itNgrams.hasNext()) 
			{
				NGram ngram = (NGram) itNgrams.next();
				int countFuture = ngramTrained.cmNGramCounts.get(ngram).intValue();
				totalcountfuture = totalcountfuture + countFuture;
				int countHistory = 0;
				if (i==1)
				{
					countHistory = segments;    
				}
				else
				{
					if (ngram.backoff().isStart())
					{
						countHistory = sentences;
					}
					else
					{
						countHistory = ngramTrainedHistory.cmNGramCounts.get(ngram.backoff()).intValue();
					}
				}
				
				double logprob = Math.log((double)countFuture/(double)countHistory);
				totalcountlog = totalcountlog + logprob;
				lwTransitions.writeLine(logprob+"\t"+ngram.toString().replaceAll(",","\t"));
			}
			lwTransitions.writeLine("");
			System.out.println(" for model "+i+" total futures "+totalcountfuture);
			System.out.println(" for model "+i+" total log "+totalcountlog);
			
		}
		
		lwTransitions.close();
	}
}
