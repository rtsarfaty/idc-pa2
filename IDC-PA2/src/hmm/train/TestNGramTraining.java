package hmm.train;

import static org.junit.Assert.assertEquals;
import java.util.ArrayList;
import org.junit.Test;

public class TestNGramTraining {

	@Test
	public void testNgrams() {
		
		String s1 = "the";
		String s2 = "cat";
		String s3 = "is";
		String s4 = "on";
		String s5 = "the";
		String s6 = "mat";
		
		ArrayList<String> seq = new ArrayList<String>();
		seq.add(s1);
		seq.add(s2);
		seq.add(s3);
		seq.add(s4);
		seq.add(s5);
		seq.add(s6);
		
		ArrayList<String> seq2 = new ArrayList<String>();
		seq2.add(s1);
		seq2.add(s2);
		seq2.add(s3);
		seq2.add(s4);
		seq2.add(s5);
		seq2.add(s6);
		
		TrainLM nGramTrainUnigrams = new TrainLM(1);
		nGramTrainUnigrams.addNGrams(seq);
		nGramTrainUnigrams.addNGrams(seq2);
		
		System.out.println(nGramTrainUnigrams.getCountMap());
		assertEquals("number of unigrams should be 5",5, nGramTrainUnigrams.getNumberOfNgrams());
		
		TrainLM nGramTrainBigrams = new TrainLM(2);
		nGramTrainBigrams.addNGrams(seq);
		nGramTrainBigrams.addNGrams(seq2);
	
		System.out.println(nGramTrainBigrams.getCountMap());
		assertEquals("number of bigrams should be 7",7, nGramTrainBigrams.getNumberOfNgrams());
	

		TrainLM nGramTrainTrigrams = new TrainLM(3);
		nGramTrainTrigrams.addNGrams(seq);
		nGramTrainTrigrams.addNGrams(seq2);
	
		System.out.println(nGramTrainTrigrams.getCountMap());
		assertEquals("number of trigrams should be 8",8, nGramTrainTrigrams.getNumberOfNgrams());
	
		TrainLM nGramTrainQuadrigrams = new TrainLM(4);
		nGramTrainQuadrigrams.addNGrams(seq);
		nGramTrainQuadrigrams.addNGrams(seq2);
	
		System.out.println(nGramTrainQuadrigrams.getCountMap());
		assertEquals("number of trigrams should be 9",9, nGramTrainQuadrigrams.getNumberOfNgrams());
	
	}

}
