package hmm.train;

import java.util.ArrayList;

public class TaggedSequence {
	
	public TaggedSequence() {
		super();
	}

	private ArrayList<TaggedTerminal> lstTaggedSequence = new ArrayList<TaggedTerminal>();
	private ArrayList<String> lstTags = new ArrayList<String>();
	public ArrayList<TaggedTerminal> getTaggedSequence() {
		return lstTaggedSequence;
	}

	public void setTaggedSequence(ArrayList<TaggedTerminal> lstTaggedSequence) {
		this.lstTaggedSequence = lstTaggedSequence;
	}
	
	public void add(TaggedTerminal tt)
	{
		getTaggedSequence().add(tt);
		getTags().add(tt.getPOStag());
	}
	
	public String toString()
	{
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < getTaggedSequence().size(); i++) {
			sb.append(getTaggedSequence().get(i));
			sb.append("\n");
		}
		return sb.toString();
	}

	public ArrayList<String> getTags() {
		return lstTags;
	}
	

	/*public void add(String sTerminal, String sTag)
	{
		getTaggedSequence().add(new TaggedTerminal(sTerminal, sTag));
		getTags().add(sTag);
	}*/

}
