package hmm.train;

public class TaggedTerminal implements Comparable<TaggedTerminal> {

	public TaggedTerminal(String sTerminal, String sPOStag) {
		super();
		this.sTerminal = sTerminal;
		this.sPOStag = sPOStag;
	}
	
	private String sTerminal = null;
	private String sPOStag = null;
	
	public String getTerminal() {
		return sTerminal;
	}
	public void setTerminal(String sTerminal) {
		this.sTerminal = sTerminal;
	}
	public String getPOStag() {
		return sPOStag;
	}
	public void setPOStag(String sPOStag) {
		this.sPOStag = sPOStag;
	}
	
	public String toString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append(sTerminal);
		sb.append("\t");
		sb.append(sPOStag);
		
		return sb.toString();
	}

	public boolean equals(Object o)
	{
		return this.toString().equals(((TaggedTerminal)o).toString());
	}
	
	@Override
	public int compareTo(TaggedTerminal o) {
		return this.toString().compareTo(o.toString());
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		String s = toString();
		result = prime * result + s.hashCode();
		return result;
	}

}
