package hmm.train;

import hmm.NGram;

import java.util.ArrayList;

import utils.CountMap;

public class TrainLM {

	public TrainLM(int intNGramModel) {
		super();
		this.intNGramModel = intNGramModel;
	}

	private int intNGramModel = 0;
	CountMap<NGram> cmNGramCounts = new CountMap<NGram>();

	public int getN() {
		return intNGramModel;
	}

	public void setN(int intNGramModel) {
		this.intNGramModel = intNGramModel;
	}
	
	public int getNumberOfNgrams()
	{
		return cmNGramCounts.size();
	}
	
	public CountMap<NGram> getCountMap()
	{
		return cmNGramCounts;
	}
	
	public void addNGrams(ArrayList<String> sequence)
	{
		CountMap<NGram> ngrams = cmNGramCounts;
		
		ArrayList<String> seq = new ArrayList<String>();
		seq.addAll(sequence);
		
		// pad the start / end with symbols
		for (int i = 0; i < getN()-1; i++) {
			seq.add(0,NGram._start);
			seq.add(NGram._end);
		}
		
		// create a window and loop through
		for (int i = 0; i < seq.size()-(getN()-1); i++) {
			NGram n = new NGram(getN());
			for (int j = 0; j < getN(); j++) {
				n.add((String)seq.get(i+j));
			}
			ngrams.add(n,1);
		}
	}
}
