package hmm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
 
import utils.MapMap;
import utils.LineReader;
import utils.LineWriter; 
public class DecodeHMM {

	public static void main(String[] args) {
		
	
		if (args.length < 3) 
		{
			System.err.println("Usage:  TrainHMM  <test> <lex> <gram>");
		} 
		else 
		{
			// read command line args
			String sLex =  args[1];
			String sGram = args[2];
			String sTest = args[0];

			// read emissions
			MapMap<Symbol, String, Double> theEmissions = readEmissions(sLex);
			
			// read transitions 
			Map<Integer, MapMap<String, State, Double>> allTransitions = readTransitions(sGram);
			Integer  theModelOrder = new Integer(allTransitions.size());
			MapMap<String, State, Double> theTransitions = allTransitions.get(theModelOrder);
			
			// read states
			List<State> lstStates = readStatesFromTransitions(theModelOrder,theTransitions);
			
			// read input
			List<List<Symbol>> theTestSet = readInput(sTest);
			
			// tag input and write output
			HMM hmm = new HMM(theTransitions, theEmissions, lstStates);
			List<List<State>> theBestSet = new ArrayList<List<State>>();
			for (int i = 0; i < theTestSet.size(); i++) {
				Viterbi v = new Viterbi(hmm, theTestSet.get(i));
				v.decode();
				List<State> s= v.getBestStateSequence();
				theBestSet.add(s);
				System.out.println(s.toString());
			}
			
			// write output
			writeOutput(sTest+".tagged", theTestSet, theBestSet);
		}
	}

	private static void writeOutput(String sTest,
			List<List<Symbol>> theTestSet, List<List<State>> lstBest) 
	{
		LineWriter taggedset = new LineWriter(sTest);
		for (int j = 0; j < theTestSet.size(); j++) {
			List<Symbol> lstInput = theTestSet.get(j);
			List<State> lstOutput = lstBest.get(j);
			for (int i = 0; i < lstInput.size(); i++) {
				taggedset.writeLine(lstInput.get(i).toString()+"\t"+lstOutput.get(i+1).toString());
			}
			if (j<theTestSet.size()-1)
				taggedset.writeLine("");	
		}
		
		taggedset.close();
	}

	public static List<State> readStatesFromTransitions(Integer theModelOrder,
			MapMap<String, State, Double> theTransitions) 
	{
		System.out.println("Reading States for Ngrams="+theModelOrder);
		// get state for the right order
		Set<State> allStates = new HashSet<State>();
		Iterator<String> itMaps = theTransitions.keySet().iterator();
		while (itMaps.hasNext()) {
			String tag = (String) itMaps.next();
			allStates.addAll(theTransitions.get(tag).keySet());
		}
		
		// create a start state and end state for the model
		NGram ngramStart = new NGram(theModelOrder.intValue());
		for (int i = 0; i < theModelOrder.intValue(); i++) {
			ngramStart.add(NGram._start);
		}
		NGram ngramEnd = new NGram(theModelOrder.intValue());
		for (int i = 0; i < theModelOrder.intValue(); i++) {
			ngramEnd.add(NGram._end);
		}
		State startState = new State(ngramStart.toString());
		State endState = new State(ngramEnd.toString());
		
		// add states to ordered lisr
		List<State> lstStates  = new ArrayList<State>();
		Iterator<State> itSt = allStates.iterator();
		while (itSt.hasNext()) {
			State state = (State) itSt.next();
			if (!state.equals(startState) && !state.equals(endState))
			{
				boolean b = lstStates.add(state);
				if (b) System.out.println("Adding State:"+state);
			}
		}
		
		lstStates.add(0,startState);
		System.out.println("Adding State:"+startState); 
		
		lstStates.add(endState);
		System.out.println("Adding State:"+endState); 
		
		System.out.println("Print States:"+lstStates); 
		
		return lstStates;
	}

	public static Map<Integer, MapMap<String, State, Double>> readTransitions(
			String sGram) {
		System.out.println("Reading Transitions...");
		String sLine;
		MapMap<String, State, Double> theMap  = null;
		/*
		 * \data\ 
		 * ngram 1 = <number of unigrams> 
		 * ngram 2 = <number of bigrams> 
		 * ngram 3 = <number of trigrams> ...
		 *
		 * \1-grams\ 
		 * logprob POS 
		 * logprob POS ... 
		 * 
		 * \2-grams\ 
		 * logprob POS POS 
		 * logprob POS POS ...
		 * 
		 */
		
		LineReader grammatical = new LineReader(sGram);
		sLine = grammatical.readLine();	
		
		boolean bNewNgram = false;
		int iInNgram = 0;
		
		Map<Integer, MapMap<String, State, Double>>  allTransitions = new HashMap<Integer, MapMap<String, State, Double>>();
		
		while (sLine != null) {
			
			String sInput = sLine.trim();
			//System.out.println("reading line:"+sLine.trim());
			
			if (sInput.length() == 0 || sInput.startsWith("#"))
			{	
				System.out.println("skipping line:"+sInput);
				sLine = grammatical.readLine();	
				continue;
			}
			if (sInput.equals("\\data\\"))
			{
				System.out.println("In \\data\\ header line:"+sInput);
				sLine = grammatical.readLine();	
				continue;
			}
			if (sInput.startsWith("ngram "))
			{
				System.out.println("In \\data\\ header line:"+sInput);
				sLine = grammatical.readLine();	
				continue;
			}
			if (sInput.endsWith("-grams\\"))
			{
				bNewNgram = true;
			    iInNgram = Integer.valueOf(sInput.substring(1, 2));
				System.out.println("In ngrams line: n ="+iInNgram);
				sLine = grammatical.readLine();	
			    continue;
			}
			
			
			// process logprobs
			if (bNewNgram)
			{
				theMap = new MapMap<String, State, Double>();
				allTransitions.put(new Integer(iInNgram), theMap );
				bNewNgram = false;
				continue;
				// add it to a map of mapmaps
			}
			else
			{
				//System.out.println("input:"+sInput);
				sInput = sInput.replaceAll(",","\t");
				StringTokenizer st = new StringTokenizer(sInput, "\t");
				if (st.countTokens() < 2)
					throw new IllegalStateException(" no n-gram specified  in line " + sInput);
				String sLogProb = st.nextToken();
				NGram ngram = new NGram(iInNgram);
				for (int i = 0; i < iInNgram; i++) {
					String sWord = st.nextToken();
					ngram.add(sWord);
				}
				String theTag = ngram.getSequence().get(iInNgram-1);
				State theState = new State(ngram.toString());
				//System.out.println("entering transition of " +iInNgram +"-gram: "+theTag+ " " + theState + " " + sLogProb);
				theMap.put(theTag,  theState, new Double(sLogProb));
			}
			sLine = grammatical.readLine();	
		}
		return allTransitions;
	}

	public static List<List<Symbol>> readInput(String sLex) {
		
		
		List<List<Symbol>> theTestSet = new ArrayList<List<Symbol>>();
		LineReader testset = new LineReader(sLex);
		String sLine = testset.readLine();	
		
		ArrayList<Symbol> theSentence = new ArrayList<Symbol>();
		int counter = 0;
	
		while (sLine != null) {
			if (sLine.startsWith("#"))
			{
				System.out.println("skipping line:"+sLine);
				sLine = testset.readLine();	
			}
			else if (sLine.trim().length() == 0)
			{
				System.out.println("skipping empty");
					if (theSentence.size()>0)
					{
						//System.out.println("adding sent"+theSentence);
						theTestSet.add(theSentence);
						counter++;
					}
					theSentence = new ArrayList<Symbol>();
					sLine = testset.readLine();	
			}
			else 
			{	
				System.out.println("adding "+sLine.trim());
				theSentence.add(new Symbol(sLine.trim()));
				sLine = testset.readLine();	
			}
		}
		if (theSentence.size()>0)
		{
			//System.out.println("adding sent"+theSentence);
			theTestSet.add(theSentence);
			counter++;
		}
	
		System.out.println("added "+counter +" test sentences");
		return theTestSet;
	}

	public static MapMap<Symbol, String, Double> readEmissions(String sLex) {
		MapMap<Symbol, String, Double> theEmissions = new MapMap<Symbol, String, Double>();
		LineReader lexical = new LineReader(sLex);
		String sLine = lexical.readLine();	

		while (sLine != null) {
			StringTokenizer st = new StringTokenizer(sLine.trim(),"\t");
			if (sLine.startsWith("#")|| st.countTokens() < 3)
			{
				//System.out.println("count tokens : "+ st.countTokens());
				System.out.println("skipping line:"+sLine);
				sLine = lexical.readLine();	
				continue;
			}
			Symbol symSeg = new Symbol(st.nextToken());
			while (st.hasMoreTokens()) {
				String sPos = st.nextToken();
				String sLogProb = st.nextToken();
				if (sPos == null || sLogProb == null)
						throw new IllegalStateException("No POS/Prob is specificied for segment "+symSeg);
				if (theEmissions.get(symSeg) == null)
					theEmissions.put(symSeg, new HashMap<String, Double>());
				//System.out.println("entering emission: "+symSeg+ "\t" +sPos +"\t" +sLogProb);
				theEmissions.put(symSeg, sPos, new Double(sLogProb));
			}
			sLine = lexical.readLine();	
		}
		return theEmissions;
	}
}
