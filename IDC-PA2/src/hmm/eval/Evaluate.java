package hmm.eval;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import utils.LineReader;
import utils.LineWriter;

public class Evaluate {

	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String sGold = args[0];
		String sTest= args[1];
		String string= args[2];
		
		List<List<String>> theGoldData = new ArrayList<List<String>>();
		readTaggedSequence(sGold, theGoldData);
		
		List<List<String>> theTestData = new ArrayList<List<String>>();
		readTaggedSequence(sTest, theTestData);
		
		evaluate(sGold+string+".eval", theGoldData, theTestData);

	}

	private static void readTaggedSequence(String sGold,
			List<List<String>> theData) {
		List<String> theGold = new ArrayList<String>();
		LineReader goldset = new LineReader(sGold);
		String sLine = goldset.readLine();
		int sents = 0;
		
		while (sLine != null) {
			sLine = sLine.trim();
			System.out.println("["+sLine+"]");
			if (sLine.length() == 0)
			{
				// empty line --> interpret as sentence
				theData.add(theGold);
				sents++;
				// start new sentence
				theGold = new ArrayList<String>();
				sLine = goldset.readLine();
				continue;
			}
			/*else if (sLine.startsWith("#"))
			{
				// skip line 
				sLine = goldset.readLine();
				continue;
			} */
			else
			{
				// interpret line
				StringTokenizer st = new StringTokenizer(sLine,"\t");
				if (st.countTokens() != 2) 
				{
					System.err.println("number of tokens should be 2");
					throw new IllegalStateException("number of strings per line should be 2: "+sLine);
				}
				
				String tag = null;
				tag = st.nextToken();
				tag = st.nextToken();
				//System.out.println("adding tags "+tag);
				theGold.add(tag);
				sLine = goldset.readLine();
			}
		}
		// empty line --> interpret as sentence
		if (!theGold.isEmpty())
		{
			theData.add(theGold);
			sents++;
		}
		System.out.println("processed "+sents);
		

	}

	public double  eval(List<String> m_lstGold, List<String> m_lstTagged)
	{
		if (m_lstGold.size() != m_lstTagged.size())
			throw new IllegalStateException("Evaluate(): Length unmatched."+ m_lstGold + m_lstTagged);
		
		int correct = 0;
		for (int i = 0; i < m_lstGold.size(); i++) {
			if (m_lstGold.get(i).equals(m_lstTagged.get(i)))
				correct++;
		}
		
		return ((double)correct)/((double)(m_lstGold.size()));
	}
	

	public static void  evaluate(String sName, List<List<String>> m_lstGoldSents, List<List<String>> m_lstTaggedSents)
	{
		LineWriter lw = new LineWriter(sName);
		lw.writeLine("### pos evaluation ### "+sName + " ###");
		
		if (m_lstGoldSents.size() != m_lstTaggedSents.size())
			throw new IllegalStateException("Evaluate(): #sents unmatched."+ m_lstGoldSents.size() + " / "+ m_lstTaggedSents.size());
		
		double correct = 0;
		double exact = 0;
		double total = 0;
		
		for (int i = 0; i < m_lstGoldSents.size(); i++) {
			
			List<String> theGold = m_lstGoldSents.get(i);		
			List<String> theTagged = m_lstTaggedSents.get(i);
			if (theGold.size() != theTagged.size())
				throw new IllegalStateException("Evaluate(): #length unmatched."+ theGold.size() + " / "+ theTagged.size());
			
			total = total+theGold.size();
			
			double localcorrect = 0;
			double localexact = 0;
			double localtotal = theGold.size();
					
			for (int j = 0; j < theGold.size();j++) {
				System.out.println(theGold.get(j)+" compared to "+ theTagged.get(j)+ " " );
				if (theGold.get(j).equals(theTagged.get(j)))
				localcorrect++;
			}
			if (localcorrect == theGold.size())
				localexact++;
			
			lw.writeLine(i+
				"\t"
				+((double)localcorrect / (double)localtotal) + 
				"\t"
				+ ((double)localexact));
			correct = correct + localcorrect;
			exact = exact+localexact;
		}
		lw.writeLine("----------------------------");
		lw.writeLine("avg:"+
				"\t"
				+((double)correct / (double)total) + 
				"\t"
				+ ((double)exact / (double)m_lstGoldSents.size()));
		
		lw.close();
	}
}
