package hmm;

public class Emission {

	public Emission(State sState, Symbol oSymbol, int iProbability) {
		super();
		this.sState = sState;
		this.oSymbol = oSymbol;
		this.iProbability = iProbability;
	}
	
	private State sState = null;
	private Symbol oSymbol = null;
	private long iProbability = 0;
	
	public State getState() {
		return sState;
	}
	public void setState(State sState) {
		this.sState = sState;
	}
	public Symbol getSymbol() {
		return oSymbol;
	}
	public void setSymbol(Symbol oSymbol) {
		this.oSymbol = oSymbol;
	}
	public long getProbability() {
		return iProbability;
	}
	public void setProbability(long iProbability) {
		this.iProbability = iProbability;
	}
		
}
