package hmm;

import hmm.train.TaggedSequence;
import hmm.train.TaggedTerminal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class Viterbi {
	
	
	public Viterbi(HMM theModel, List<Symbol> theInput) {
		super();
		int length = theInput.size();
		int states = theModel.getStates().size();
		setInput(theInput);
		setNumOfStates(states);
		setObservationLength(length);
		setVIJ(new double[length+2][states]);
		setBIJ(new int[length+2][states]);
		setModel(theModel);
	}
	

	private HMM hmmModel = null ;
	private List<Symbol> lstInput = null;
	private List<State> lstBestStateSequence =  null;
	
	private double[][] arrayVIJ = null;
	private int[][] arrayBIJ = null;

	private int iObservations = 0;
	private int iStates = 0;
	
	public double[][] getVIJ() {
		return arrayVIJ;
	}
	public void setVIJ(double[][] arrayVIJ) {
		this.arrayVIJ = arrayVIJ;
	}

	
	public HMM getModel() {
		return hmmModel;
	}

	public void setModel(HMM hmmModel) {
		this.hmmModel = hmmModel;
	}
	
	public int getNumOfStates() {
		return iStates;
	}

	public void setNumOfStates(int iStates) {
		this.iStates = iStates;
	}

	
	// a decoding algorithm for HMMs
	
	public List<State> getBestStateSequence() {
		return lstBestStateSequence;
	}

	public void setBestStateSequence(List<State> lstBestStateSequence) {
		this.lstBestStateSequence = lstBestStateSequence;
	}

	public void decode()
	{	
		System.out.println("decoding "+getInput()); 
		// decode time 1
		for (int sTo = 1; sTo < getNumOfStates()-1; sTo++) {
			getVIJ()[1][sTo]= 
					getModel().getTransitionProb(0,sTo)+
					getModel().getEmmissionProb(sTo,getInput().get(0));
			//System.out.println("transition "+ getModel().getStates().get(0)+ " " +getModel().getStates().get(sTo)+" " +getModel().getTransitionProb(0,sTo));
			//System.out.println("emission " +getModel().getStates().get(sTo) + "->" + getInput().get(0) +" " + getModel().getEmmissionProb(sTo,getInput().get(0)));
			//System.out.println("initiating [1,"+getInput().get(0)+"] ="+getVIJ()[1][sTo]);
			getBIJ()[1][sTo] = 0;
		}
		// decode 1<time<length		
		for (int time = 2; time < getObservationLength()+1; time++) {
			for (int sTo = 1; sTo < getNumOfStates(); sTo++) {
				System.out.println("filling ["+time+","+sTo+"]");
				getVIJ()[time][sTo] = Double.NEGATIVE_INFINITY;
				for (int sFrom = 1; sFrom < getNumOfStates()-1; sFrom++) {
					if (!isValidTransition(sFrom,sTo))
						continue;
					double current = getVIJ()[time-1][sFrom]+
							getModel().getTransitionProb(sFrom, sTo)+
							getModel().getEmmissionProb(sTo,getInput().get(time-1));
					//System.out.println(" in time "+time+" going from " +sFrom+ " to "+sTo+" = "+current);			
					if (current> getVIJ()[time][sTo])
					{
						System.out.println("updating to state "+sFrom);
						getVIJ()[time][sTo] = current;
						getBIJ()[time][sTo] = sFrom;
					}
				}	
			}
		}
		// decode time length+1
		int time = getObservationLength()+1; 
		int sTo = getNumOfStates()-1;
		getVIJ()[time][sTo] = Double.NEGATIVE_INFINITY;
		System.out.println("filling ["+time+","+sTo+"]");
		for (int sFrom = 1; sFrom < getNumOfStates()-1; sFrom++) {
				double current = 
						getVIJ()[time-1][sFrom]+
						getModel().getTransitionProb(sFrom,sTo);
				//System.out.println(" in time "+time+" going from " +sFrom+ " to "+sTo+" = "+current);
				if (current>getVIJ()[time][sTo])
				{
					System.out.println("updating to state "+sFrom);
					getVIJ()[time][sTo]= current;
					getBIJ()[time][sTo] = sFrom;
					//System.out.println("initiating ["+time+","+sTo+"] ="+getVIJ()[time][sTo]+" from state "+sFrom);
				}
			}
		/*
		System.out.println("-------> BIJ =");
		for (int j = 0; j < getObservationLength()+2; j++) {	
			for (int i = 1; i < getNumOfStates(); i++) {
				System.out.print(j+","+i+"= "+getBIJ()[j][i]+" / ");
			}
			System.out.println();
		}*/
		
		//System.out.println(" readoff ...");
		// readoff time n+1
		setBestStateSequence(new ArrayList<State>(getObservationLength()+2));
		State sCurrent = getModel().getStates().get(getNumOfStates()-1);
		getBestStateSequence().add(sCurrent);
		int back = getBIJ()[getObservationLength()+1][getNumOfStates()-1];
		
		// readoff time 0<i<n
		for (int i = getObservationLength(); i>=0; i--) {
			sCurrent = getModel().getStates().get(back);
			getBestStateSequence().add(sCurrent);
			back = getBIJ()[i][back];
		}
		
		Collections.reverse(getBestStateSequence());
	}
	
	public TaggedSequence returnTaggedSequence()
	{
		//ArrayList<String> theSegs = new ArrayList<String>();
		//ArrayList<String> theTags = new ArrayList<String>();
		
		TaggedSequence ts = new TaggedSequence();
		for (int i = 0; i < getInput().size(); i++) {
			TaggedTerminal tt = new TaggedTerminal(
					getInput().get(i).getID(), 
					getBestStateSequence().get(i+1).getID());
			ts.add(tt);
		}
		
		return ts;
	}
	
	/*
	public void decode(int time)
	{
		for (int s = 1; s < iStates; s++) 
				decode(time,s);
	}
	public void decode(int time, int sTo)
	{
		getVIJ()[time][sTo] = Double.NEGATIVE_INFINITY;
		for (int sFrom = 1; sFrom < iStates-1; sFrom++) {
			if (!isValidTransition(sFrom,sTo))
				continue;
			double current = getVIJ()[time-1][sFrom]+
					getModel().getTransitionProb(sFrom, sTo)+
					getModel().getEmmissionProb(sTo,getModel().getSymbols().get(time-1));
			System.out.println(" in time "+time+" going from " +sFrom+ " to "+sTo+" = "+current);
			
			if (current> getVIJ()[time][sTo])
			{
				System.out.println("updating to state "+sFrom);
				getVIJ()[time][sTo] = current;
				getBIJ()[time][sTo] = sFrom;
			}
		}	
	}
	
	*/

	private boolean isValidTransition(int sFrom, int sTo) {
		
		List<String> arrFrom = getModel().getStates().get(sFrom).getSequence();
		List<String> arrTo =  getModel().getStates().get(sTo).getSequence();
		
		// no overlap
		if (arrFrom.size() == 1 && arrTo.size() == 1)
			return true;
					
		// overlap exists -- 
		// check indentity of n-1 symbols
		arrFrom.remove(0);
		arrTo.remove(arrTo.size()-1);
		
		for (int i = 0; i < arrFrom.size(); i++) {
			if (!arrFrom.get(i).equals(arrTo.get(i)))
				return false;
		}
		return true;
	}

	public int[][] getBIJ() {
		return arrayBIJ;
	}

	public void setBIJ(int[][] arrayBIJ) {
		this.arrayBIJ = arrayBIJ;
	}

	public int getObservationLength() {
		return iObservations;
	}

	public void setObservationLength(int iObservations) {
		this.iObservations = iObservations;
	}
	public List<Symbol> getInput() {
		return lstInput;
	}
	public void setInput(List<Symbol> lstInput) {
		this.lstInput = lstInput;
	}

		
}
