package hmm;

public class Symbol {

	public static String _unk = "###UNK###";
	public Symbol(String sID) {
		super();
		this.sID = sID;
	}

	private String sID = null;
	
	public String getID() {
		return sID;
	}

	public void setID(String sID) {
		this.sID = sID;
	}
	

	@Override
	public String toString()
	{
		return getID();
	}
	
	@Override
	public boolean equals(Object o)
	{
		return this.toString().equals(((Symbol)o).toString());
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		String s = toString();
		result = prime * result + s.hashCode();
		return result;
	}
	
}
