package hmm;

public class Transition {
	
	public Transition(State stateFrom, State stateTo, int iProbability) {
		super();
		this.stateFrom = stateFrom;
		this.stateTo = stateTo;
		this.iProbability = iProbability;
	}
	
	private State stateFrom = null;
	private State stateTo = null;
	private int iProbability = 0;
	
	public State getFrom() {
		return stateFrom;
	}
	public void setFrom(State stateFrom) {
		this.stateFrom = stateFrom;
	}
	
	public State getTo() {
		return stateTo;
	}
	public void setTo(State stateTo) {
		this.stateTo = stateTo;
	}
	public int getProbability() {
		return iProbability;
	}
	public void setProbability(int iProbability) {
		this.iProbability = iProbability;
	}
}

