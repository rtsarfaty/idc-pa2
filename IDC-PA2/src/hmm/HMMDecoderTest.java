package hmm;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Test;


import utils.MapMap;
import  utils.LineWriter;
public class HMMDecoderTest {


	public void testTriGramTagger() {
		assert(true);
	}
	
	@Test
	public void testBiGramTagger() {
		
		MapMap<String, State, Double> mapTransitions = new MapMap<String, State, Double>();
		MapMap<Symbol, String, Double> mapEmissions = new MapMap<Symbol, String, Double>();
		List<State> lStates = new ArrayList<State>();
		List<Symbol> lSymbols = new ArrayList<Symbol>();
		 
		// states
		
		State stateS = new State(NGram._start);
		State stateE = new State(NGram._end);
		
		State state1 = new State("VB");
		State state2 = new State("TO");
		State state3 = new State("NN");
		State state4 = new State("PPSS");
		
		lStates.add(stateS);
		lStates.add(state1);
		lStates.add(state2);
		lStates.add(state3);
		lStates.add(state4);
		lStates.add(stateE);
		
		Symbol sI = new Symbol("I");
		Symbol sWant = new Symbol("want");
		Symbol sTo = new Symbol("to");
		Symbol sRace = new Symbol("race");
		
		lSymbols.add(sI);
		lSymbols.add(sWant);
		lSymbols.add(sTo);
		lSymbols.add(sRace);
		
		mapTransitions.put("VB", stateS,  new Double(Math.log(0.019)));
		mapTransitions.put("VB", state1,  new Double(Math.log(0.0038)));
		mapTransitions.put("VB", state2,  new Double(Math.log(0.38)));
		mapTransitions.put("VB", state3,  new Double(Math.log(0.0040)));
		mapTransitions.put("VB", state4,  new Double(Math.log(0.23)));
		
		mapTransitions.put("TO", stateS,  new Double(Math.log(0.0043)));
		mapTransitions.put("TO", state1,  new Double(Math.log(0.035)));
		mapTransitions.put("TO", state2,  new Double(Math.log(0.0)));
		mapTransitions.put("TO", state3,  new Double(Math.log(0.016)));
		mapTransitions.put("TO", state4,  new Double(Math.log(0.00079)));

		mapTransitions.put("NN", stateS,  new Double(Math.log(0.041)));
		mapTransitions.put("NN", state1,  new Double(Math.log(0.047)));
		mapTransitions.put("NN", state2,  new Double(Math.log(0.00047)));
		mapTransitions.put("NN", state3,  new Double(Math.log(0.087)));
		mapTransitions.put("NN", state4,  new Double(Math.log(0.0012)));

		mapTransitions.put("PPSS", stateS,  new Double(Math.log(0.67)));
		mapTransitions.put("PPSS", state1,  new Double( Math.log(0.0070)));
		mapTransitions.put("PPSS", state2,  new Double(Math.log(0.0)));
		mapTransitions.put("PPSS", state3,  new Double(Math.log(0.0045)));
		mapTransitions.put("PPSS", state4,  new Double(Math.log(0.00014)));
		
		mapTransitions.put(NGram._end, stateS,  new Double(Math.log(0.0001)));
		mapTransitions.put(NGram._end, state1,  new Double( Math.log(0.0001)));
		mapTransitions.put(NGram._end, state2,  new Double(Math.log(0.0001)));
		mapTransitions.put(NGram._end, state3,  new Double(Math.log(0.0001)));
		mapTransitions.put(NGram._end, state4,  new Double(Math.log(0.0001)));
		
		mapEmissions.put(sI, "VB",  new Double(Math.log(0.0)));
		mapEmissions.put(sI, "TO",  new Double(Math.log(0.0)));
		mapEmissions.put(sI, "NN",  new Double(Math.log(0.0)));
		mapEmissions.put(sI, "PPSS",  new Double(Math.log(0.37)));
		mapEmissions.put(sI, NGram._end,  new Double(Math.log(0.0)));
		
		mapEmissions.put(sWant, "VB",  new Double(Math.log(0.0093)));
		mapEmissions.put(sWant, "TO",  new Double(Math.log(0.0)));
		mapEmissions.put(sWant, "NN",  new Double(Math.log(0.0000054)));
		mapEmissions.put(sWant, "PPSS", new Double(Math.log(0.0)));
		mapEmissions.put(sWant, NGram._end,  new Double(Math.log(0.0)));
		
		mapEmissions.put(sTo, "VB",  new Double(Math.log(0.0)));
		mapEmissions.put(sTo, "TO",  new Double(Math.log(0.99)));
		mapEmissions.put(sTo, "NN",  new Double(Math.log(0.0)));
		mapEmissions.put(sTo, "PPSS",  new Double(Math.log(0.0)));
		mapEmissions.put(sTo, NGram._end,  new Double(Math.log(0.0)));
		
		mapEmissions.put(sRace, "VB",  new Double(Math.log(0.00012)));
		mapEmissions.put(sRace, "TO",  new Double(Math.log(0.0)));
		mapEmissions.put(sRace, "NN",  new Double(Math.log(0.00057)));
		mapEmissions.put(sRace, "PPSS",  new Double(Math.log(0.0)));
		mapEmissions.put(sRace, NGram._end,  new Double(Math.log(0.0)));
		
		HMM hmm = new HMM(
				mapTransitions, 
				mapEmissions, 
				lStates); 
		
		Viterbi viterbi = new Viterbi(hmm,lSymbols);
		viterbi.decode();
		
		System.out.println(viterbi.returnTaggedSequence());
		String result ="I\tPPSS\nwant\tVB\nto\tTO\nrace\tVB\n";
		System.out.println(result);
		assert(viterbi.returnTaggedSequence().equals(result));
	}

	@Test
	public void testBiGramTaggerOnTestFiles() {
		
			// read command line args
			String sLex =  "/Users/admin/IDC-HW/pa1/heb-pos.gold-2-n.lex";
			String sGram = "/Users/admin/IDC-HW/pa1/heb-pos.gold-2-n.gram";
			String sTest = "/Users/admin/IDC-HW/pa1/heb-pos.test";

			// read emissions
			MapMap<Symbol, String, Double> theEmissions = DecodeHMM.readEmissions(sLex);
			
			// read transitions for each n-order
			Map<Integer, MapMap<String, State, Double>> allTransitions = DecodeHMM.readTransitions(sGram);
			Integer  theModelOrder = new Integer(allTransitions.size());
			MapMap<String, State, Double> theTransitions = allTransitions.get(theModelOrder);
			
			// read states
			List<State> lstStates = DecodeHMM.readStatesFromTransitions(theModelOrder,theTransitions);
			
			// read input
			List<List<Symbol>> theTestSet = DecodeHMM.readInput(sTest);
			
			System.out.println("states "+lstStates.size());
			System.out.println("emissions "+theEmissions);
			System.out.println("transitions "+theTransitions);
			
			// write tagged output
			HMM hmm = new HMM(theTransitions, theEmissions, lstStates);
			LineWriter taggedset = new LineWriter(sTest+".tagged");
		
			for (int i = 0; i < theTestSet.size(); i++) {
				System.out.println("decode "+theTestSet.get(i));
				Viterbi v = new Viterbi(hmm, theTestSet.get(i));
				v.decode();
				taggedset.writeLine(v.getBestStateSequence().toString());
				System.out.println(v.getBestStateSequence().toString());
				
			}
			taggedset.close();
			
			assert(true);
	}

}
