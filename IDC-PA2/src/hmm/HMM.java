package hmm;

import utils.MapMap;
import hmm.Symbol;

import java.util.List;
import java.util.Map;


public class HMM {
	
	public HMM( 
			MapMap<String,State,Double> mapTransitions,
			MapMap<Symbol, String, Double> mapEmissions, 
			List<State> lstStates)
	{
		super();
		this.setTransitions(mapTransitions);
		this.setEmissions(mapEmissions);
		this.setStates(lstStates);
		
	}

	// A fixed list of possible states
	private List<State> lstStates = null;
	// Map of estimates of transition probabilities toTAG -> fromSTATE -> TRANS-PROB
	private MapMap<String, State, Double> mapmapTransitions = null;
	// Map of estimates of emission probabilities SEG -> toTAG -> EMISSION-PROB
	private MapMap<Symbol, String, Double> mapmapEmissions = null;
	
	// a decoding algorithm for HMMs
	
	public double getEmmissionProb(int sTo, Symbol observation) {
		State state = getStates().get(sTo); 
		String sTag = state.getLastInSequence();
		//System.out.println("looking for log-probability " +
		//		"word <"+observation+">  in <" +sTag+">" );
		if (getEmissions().get(observation, sTag) == null)
		{
			//System.out.println("looking for log-probability " +
				//		"word <"+observation+">  in <" +sTag+"> = -infinity" );
			return Double.NEGATIVE_INFINITY;
		//	throw new IllegalStateException("cannot find emission log-probability " +
		//									"for observation <"+observation+","+sTag+">");
		}
		//System.out.println("looking for log-probability " +
		//		"word <"+observation+">  in <" +sTag+"> = " +getEmissions().get(observation, sTag).doubleValue());
		return getEmissions().get(observation, sTag).doubleValue();
		
	}

	public double getTransitionProb(int sFrom, int sTo) {
		
		State stateFrom = getStates().get(sFrom); 
		State stateTo = getStates().get(sTo); 
		String sTag = stateTo.getLastInSequence();
		if (getTransitions().get(sTag) == null)
				throw new IllegalStateException("cannot find transition log-probability " +
						"for tag <"+sTag+">");
		Map<State, Double> m = getTransitions().get(sTag);
		if (m.get(stateFrom) == null)
		{
			//System.out.println("looking for log-probability " +
				//	"from <"+stateFrom+">  to <" +stateTo+"> = -infinity");
					//+ getTransitions().get(sTag, stateFrom).doubleValue());
			return Double.NEGATIVE_INFINITY;
			//throw new IllegalStateException("cannot find transition log-probability " +
				//	"for tag <"+sTag+">  from history <" +stateFrom.toString()+">" );
		//System.out.println("looking for log-probability " +
		//		"from <"+stateFrom+">  to <" +stateTo+"> =" + getTransitions().get(sTag, stateFrom).doubleValue());
		}
		return getTransitions().get(sTag, stateFrom).doubleValue();	
	}

	public List<State> getStates() {
		return lstStates;
	}

	public void setStates(List<State> lstStates) {
		this.lstStates = lstStates;
	}

	public MapMap<String, State, Double> getTransitions() {
		return mapmapTransitions;
	}

	public void setTransitions(MapMap<String, State, Double> mapmapTransitions) {
		this.mapmapTransitions = mapmapTransitions;
	}

	public MapMap<Symbol, String, Double> getEmissions() {
		return mapmapEmissions;
	}

	public void setEmissions(MapMap<Symbol, String, Double> mapmapEmissions) {
		this.mapmapEmissions = mapmapEmissions;
	}
	
		
}
