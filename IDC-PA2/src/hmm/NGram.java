package hmm;

import java.util.ArrayList;

public class NGram implements Comparable<NGram>{
	
	public NGram(int intN)  {
		super();
		this.intN = intN;
	}

	public final static int UNIGRAM = 1;
	public final static int BIGRAM = 2;
	public final static int TRIGRAM = 3;
	public final static int QUADRIGRAM = 4;
	
	public final static String _start ="_START_" ;
	public final static String _end ="_END_" ;
	
	private int intN = 0 ;
	
	public boolean isStart()
	{
		for (int i = 0; i < intN; i++) {
			if (!((String)getSequence().get(i)).equals(NGram._start))
				return false;
		}
		return true;
	}
	public int getN() {
		return intN;
	}

	public void setN(int intN) {
		this.intN = intN;
	}

	private ArrayList<String>  lstSequence = new ArrayList<String>() ;
	
	public ArrayList<String> getSequence() {
		return lstSequence;
	}

	public void setSequence(ArrayList<String> lstSequence) {
		this.lstSequence = lstSequence;
	}

	public void add(String s)
	{
		if (getSequence().size() < getN())
		{
			getSequence().add(s);
			return;
		}
		throw new IllegalStateException("adding a string to a complete Ngram");
	}
	
	public NGram backoff()
	{
		NGram ngram = new NGram(getN()-1);
		for (int i = 0; i < getN()-1; i++) {
			ngram.add(this.getSequence().get(i));
		}
		return ngram;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		String s = toString();
		result = prime * result + s.hashCode();
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NGram other = (NGram) obj;
		if (this.getN() != (other.getN()))
			return false;	
		for (int i = 0; i < getN(); i++) {
			if (!this.getSequence().get(i).equals(other.getSequence().get(i)))
				return false;
		}
		return true;
	}
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		if (getN() == 1)
		{
			sb.append(this.getSequence().get(0));

		}
		else  
		{		
			int i = 0;
			for (; i < getN()-2; i++) {
			sb.append(getSequence().get(i));
			sb.append(",");
			}
			sb.append(getSequence().get(i));
		}
		return sb.toString();
	}

	@Override
	public int compareTo(NGram o) {
		return this.toString().compareToIgnoreCase(o.toString());
	}
}
