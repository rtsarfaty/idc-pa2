package hmm;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;



public class State {
	
	// name state is coding 
	// a sequence of tag-names 
	// separated by a ","
	

	public State(String sID) {
		super();
		this.sID = sID;
	}

	private String sID = null;
	private final static String sDelimiter = ",";

	public String getID() {
		return sID;
	}

	//public void setID(String sID) {
	//	this.sID = sID;
	//}
	
	public List<String> getSequence()
	{
		List<String> l = new ArrayList<String>();
		
		StringTokenizer st = new StringTokenizer(getID(), sDelimiter);
		while (st.hasMoreTokens()) {
			l.add(st.nextToken());
		}
		return l;
	}
	
	public String getLastInSequence()
	{
		List<String> l = getSequence();
		return l.get(l.size()-1);
	}
	
	boolean isValidTransition(State from)
	{
		State to = this;
		
		if (to.getSequence().size() != from.getSequence().size())
			throw new IllegalStateException("Input states are not of the same model types");
		
		if (to.getSequence().size() == 1 && from.getSequence().size() == 1)
			return true;
		else
		{
			int length = to.getSequence().size();
			
			List<String> l1 = to.getSequence().subList(1, length-1);
			List<String> l2 = to.getSequence().subList(0, length-2);
			
			for (int i = 0; i < length-1; i++) {
				if (!l1.get(i).equals(l2.get(i)))
					return false;
			}
			return true;
		}	
	}

	@Override
	public String toString()
	{
		return getID();
	}
	
	@Override
	public boolean equals(Object o)
	{
		return this.toString().equals(((State)o).toString());
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		String s = toString();
		result = prime * result + s.hashCode();
		return result;
	}
	
	
}
