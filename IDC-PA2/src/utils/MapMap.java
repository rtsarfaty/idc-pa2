package utils;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * A {@link HashMap} extension that simplifies creation of Map of Maps
 * 
 * <p>This extensions always returns a non-null value from {@link #get(Object)}</p>  
 *  
 * @author Ori Garin
 */
public final class MapMap<A,B,C> extends HashMap<A, Map<B,C>> {
	private static final long serialVersionUID = -605183752005154196L;
	private final Map<B,C> emptySet = Collections.emptyMap();
	
	public MapMap() {
		super();
	}
	public MapMap(int size) {
		super(size);
	}
	
	/**
	 * Associates the specified value with the specified key2 in the inner map associated with the specified key1 
	 * (first creating such map if it doesn't already exist) 
	 * 
	 * @param key1 key of outer map
	 * @param key2 key of inner map
	 * @param value 
	 * @return the previous value associated with <tt>key2</tt>, or
     *         <tt>null</tt> if there was no mapping for <tt>key2</tt>
	 */
	public final C put(A key1, B key2, C value) {
		Map<B,C> innermap = super.get(key1);
		if (innermap == null)
			put(key1, innermap = new HashMap<B,C>());
		return innermap.put(key2, value);
	}

	/**
	 * Gets the (inner) map under the given <code>key</code>,
	 * guaranteed to be non-null (returns empty map if key's not in the map)
	 * 
	 * @see java.util.HashMap#get(java.lang.Object)
	 */
	@Override
	public Map<B,C> get(Object key) {
		Map<B,C> innermap = super.get(key);
		return innermap == null ? emptySet : innermap; 
	}
	
	/**
	 * Gets the value associated with <tt>key2</tt> in the inner map associated with <tt>key1</tt> 
	 * @param key1 key of outer map
	 * @param key2 key of inner map
	 * @return the value associated with <tt>key2</tt> in the inner map associated with <tt>key1</tt>, or null no such (inner) map exists
	 */
	public C get(Object key1, Object key2) {
		return get(key1).get(key2);
	}
}
